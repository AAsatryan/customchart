package com.example.cleaner

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout

class CustomChart @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    var view: View = LayoutInflater.from(context).inflate(R.layout.custom_chart_view, this, false)

    init {
        addView(view)
        drawChart()
    }


    fun drawChart(i1: Float = 30f, i2: Float = 20F, i3: Float = 20F, i4: Float = 30F) {
        val first: LinearLayout = view.findViewById(R.id.first)
        val second: LinearLayout = view.findViewById(R.id.second)
        val third: LinearLayout = view.findViewById(R.id.third)
        val fourth: LinearLayout = view.findViewById(R.id.fourth)

        first.layoutParams = getLayoutParams(i1)
        second.layoutParams = getLayoutParams(i2)
        third.layoutParams = getLayoutParams(i3)
        fourth.layoutParams = getLayoutParams(i4)
    }

    private fun getLayoutParams(weight: Float): LayoutParams {
        return LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, weight)
    }
}