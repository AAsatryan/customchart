package com.example.cleaner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        generateRandomNumbers()

        window.statusBarColor = getColor(R.color.statusBar)
    }

    private fun generateRandomNumbers() {
        val shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake)
        settings.setOnClickListener {
            val i1 = (0..20).random() + 10
            val i2 = (0..20).random() + 10
            val i3 = (0..20).random() + 10
            val i4 = (100 - i1 - i2 - i3)
            chart.drawChart(i1.toFloat(), i2.toFloat(), i3.toFloat(), i4.toFloat())
            cacheContainer.startAnimation(shakeAnimation)
            coolContainer.startAnimation(shakeAnimation)
            securityContainer.startAnimation(shakeAnimation)
            batteryContainer.startAnimation(shakeAnimation)

        }
    }
}